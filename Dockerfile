FROM ubuntu
ADD ./dtw_collect_results.sh /usr/src/dtw_collect_results.sh
RUN chmod +x /usr/src/dtw_collect_results.sh
CMD ["/usr/src/dtw_collect_results.sh"]
